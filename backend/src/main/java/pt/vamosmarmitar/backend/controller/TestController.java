package pt.vamosmarmitar.backend.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("Test")
public class TestController {

    @GetMapping
    public String getHello() {
        return  "{ path: \"products\", component: \"ProductComponent\", data: { title: \"Product List\" }  }";
    }

}
