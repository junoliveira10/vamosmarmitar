import { Component, OnInit } from '@angular/core';

import { ItemMenu } from '../../model/item-menu';

import { TestService } from '../../service/test.service';
import { CartService } from '../../service/cart.service';

@Component({
  selector: 'app-cardapio',
  templateUrl: './cardapio.component.html',
  styleUrls: ['./cardapio.component.scss']
})
export class CardapioComponent {

  private size = 3;
  private pageNumber = 0;
  private filter = "";
  public menuListPage1: ItemMenu[];
  public menuListPage2: ItemMenu[];
  public menuListPage3: ItemMenu[];
  public menuListPageAdd: ItemMenu[];
  public more: boolean;

  constructor(private cartService: CartService,
              private testService: TestService) {

    this.testService.getProducts().subscribe((data: {}) => {
      console.log(data);
    });
    // this.testService.helloWorld().subscribe(
    //       s => console.log("Return:", s),
    //       e => console.log("Error:", e),
    //       () => console.log("Finally")
    //       );
    this.loadMenu();
   }

  private loadMenu() {
    this.resetPage();
    this.menuListPage1 = this.cartService.getMenu({pageNumber: this.pageNumber++, size: this.size}, this.filter);
    this.verifyHasMore(this.size, this.pageNumber);
    this.menuListPage2 = this.cartService.getMenu({pageNumber: this.pageNumber++, size: this.size}, this.filter);
    this.verifyHasMore(this.size, this.pageNumber);
    this.menuListPage3 = this.cartService.getMenu({pageNumber: this.pageNumber++, size: this.size}, this.filter);
    this.verifyHasMore(this.size, this.pageNumber);
  }

  private resetPage(){
    this.pageNumber = 0;
  }

  private verifyHasMore(size, page) {
    let pagesCount = this.cartService.getPagesCount(size);
    this.more = pagesCount > page;
  }

  public onClickMore() {
    let list = this.cartService.getMenu({pageNumber: this.pageNumber++, size: this.size}, this.filter)
    list.forEach(o => this.menuListPageAdd.push(o));
    this.verifyHasMore(this.size, this.pageNumber);
  }

  public onProcurarChange(event) {
    this.filter = event.target.value;
    this.loadMenu();
  }

}

