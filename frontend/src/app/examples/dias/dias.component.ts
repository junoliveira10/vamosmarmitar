import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dias',
  templateUrl: './dias.component.html',
  styleUrls: ['./dias.component.scss']
})
export class DiasComponent implements OnInit {

  images = ['', 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  //images = ['./assets/img/beringela.jpg', './assets/img/abacaxi.jpg', './assets/img/banana.jpg'];

  focus: any;
  focus1: any;


  constructor() { }

  ngOnInit() {
  }

}
