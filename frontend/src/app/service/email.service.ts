import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor() { }

  public helloWorld() {
    console.log("Hello Email!");
  }

}
