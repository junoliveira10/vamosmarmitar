import { Injectable } from '@angular/core';
import {PlatformLocation } from '@angular/common';

import { ItemMenu } from '../model/item-menu';



@Injectable({
  providedIn: 'root'
})
export class CartService {

    public getMenu(page, filter) {
        let initial = page.pageNumber * page.size;
        let limit = initial + page.size;

        // console.log("filter", filter);
        
        let listFiltered = !!filter 
        ? this.getItems().filter(o => {
            console.log("title", o.title);
            console.log("filter", filter);
            console.log("condition", o.title.indexOf(filter) !== -1);
            return  o.title.indexOf(filter) !== -1
        }) 
        : this.getItems();
        // console.log("list after", listFiltered);
        let pagedList = listFiltered.slice(initial, limit);
        // console.log(pagedList);
        return pagedList;
    }

    public getPagesCount(size) {
        return this.getItems().length / size;
    }

    private getItems() {
        return [   
            {    
                "id": "ID 01",
                "title": "Item menu 01",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_01.jpg",
                "added": false
            },
            {
                "id": "ID 02",
                "title": "Item menu 02",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_02.jpg",
                "added": false
            },
            {
                "id": "ID 03",
                "title": "Title 03",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_03.jpg",
                "added": false
            },
            {
                "id": "ID 04",
                "title": "Item menu 04",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_04.jpg",
                "added": false
            },
            {
                "id": "ID 05",
                "title": "Item menu 05",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_05.jpg",
                "added": false
            },
            {
                "id": "ID 06",
                "title": "Item menu 06",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_06.jpg",
                "added": false
            },
            {
                "id": "ID 07",
                "title": "Item menu 07",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_07.jpg",
                "added": false
            },
            {
                "id": "ID 08",
                "title": "Item menu 08",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_08.jpg",
                "added": false
            },
            {
                "id": "ID 09",
                "title": "Item menu 09",
                "descripton": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mollis elit id convallis blandit. Donec in elit odio. Maecenas in sapien ullamcorper, tempor velit in, tincidunt nibh. Praesent posuere dolor vel ultrices bibendum. Quisque in orci laoreet, tincidunt tellus id, posuere nunc. Sed a diam id ligula ornare efficitur eu vel odio. Suspendisse tincidunt orci in eros commodo blandit. Cras est odio, suscipit vel mi vitae, venenatis euismod justo. In tristique massa dolor, ac congue nisl iaculis nec. Praesent ultricies magna quis eros condimentum, ut congue erat aliquam. Suspendisse consequat ultricies erat, nec euismod magna ultrices ut.",
                "photo": "/assets/img/menu/item_09.jpg",
                "added": false
            }
        ];
    }

}
